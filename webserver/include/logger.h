#ifndef LOGGER_H
#define LOGGER_H
#define MAX_LOG_MESSAGE 512
#define TIME_SIZE 64
#include <time.h>
#include <stdio.h>
#include <syslog.h>
#include <string.h>
#include <structs.h>

char* logName;
FILE* logFile;

void logEvent(logData data);
void logError(const char* errorMessage);
#endif