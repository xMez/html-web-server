#ifndef CONFIGPARSER_H
#define CONFIGPARSER_H
#define MAX_STRLEN 256
#include <string.h>
#include <multipleRequests.h>
#include <logger.h>

int parseConfig(const char* filename, int *port, char *serverRoot, enum HandlingMethod *handlingMethod);

#endif