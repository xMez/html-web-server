#ifndef REQUEST_H
#define REQUEST_H

#include <string.h>
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/sendfile.h>
#include <logger.h>
#include <structs.h>
#define RES_SIZE 8192
#define BUFFER_SIZE 5120
#define HEAD_SIZE 1024
#define F_SIZE 256
#define DATE_SIZE 128

ssize_t response(ClientInfo* clientInfo, char *buffer); 
int parse_request(char *req, char **fpath, type *reqtype);
int parse_method(char *method, int *head);
char *parse_URI(char *path);
int parse_version(char *version);
int parse_headers(char *headers);
void *time_t_gm_asc(time_t *atime, char *buf);
int fsize(FILE *fp);
void *header(int req, char *size, char *moddate, char *res, int *ressize);
void *body(int req, int size, FILE *fp, char *res, int *ressize);

#endif
