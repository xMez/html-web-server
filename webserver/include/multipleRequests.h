#ifndef MULTIPLE_REQUESTS_H
#define MULTIPLE_REQUESTS_H
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <request.h>
#include <logger.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <signal.h>
#include <fcntl.h>
#include <arpa/inet.h>
#include <structs.h>
#define _XOPEN_SOURCE 500
#define BUFFER_SIZE 5120

enum HandlingMethod{DEFAULT, FORK, THREAD, PREFORK, MUX};

ClientInfo *fdPush(ClientInfo *array, int* size, ClientInfo *value, int *fdMax);
ClientInfo *fdRemove(ClientInfo *array, int* size, ClientInfo *value, int *fdMax);
void* handlerProc(void* arg);
void forkLoop(int fd);
void threadLoop(int fd);
void muxLoop(int fd);
void createRequestHandler(enum HandlingMethod handlingMethod, int fd);

#endif