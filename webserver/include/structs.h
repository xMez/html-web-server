#ifndef STRUCTS_H
#define STRUCTS_H
#include <arpa/inet.h>

typedef struct ClientInfo{
	int fd;
	char addr[INET_ADDRSTRLEN];
} ClientInfo;

typedef struct logData
{
    char *host;
    char *ident;
    char *authUser;
    char *request;
    int status;
    ssize_t bytes;
} logData;

typedef struct type
{
    int head;
    int full;
    int alive;
} type;

#endif