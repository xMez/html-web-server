#include <multipleRequests.h>

ClientInfo *fdPush(ClientInfo *array, int* size, ClientInfo *value, int* fdMax){
	// Update fdMax
	if(value->fd > *fdMax)
		*fdMax = value->fd;

	// Replace array with new memory
	ClientInfo *newArray = (ClientInfo*)malloc((*size+1)*sizeof(ClientInfo));
	for(unsigned int c = 0; c < *size; c++){
		newArray[c].fd = array[c].fd;
		strncpy(newArray[c].addr, array[c].addr, INET_ADDRSTRLEN);
	}
	newArray[*size].fd = value->fd;
	strncpy(newArray[*size].addr, value->addr, INET_ADDRSTRLEN);

	free(array);
	(*size)++;
	return newArray;
}

ClientInfo *fdRemove(ClientInfo *array, int* size, ClientInfo *value, int* fdMax){
	if(*size == 0)
		return NULL;
	*fdMax = 0;

	// Replace array with new memory
	ClientInfo *newArray = (ClientInfo*)malloc((*size-1)*sizeof(ClientInfo));
	int found = 0;
	for(unsigned int c = 0; c < *size; c++){
		if(array[c].fd != value->fd){
			newArray[c-found].fd = array[c].fd;
			strncpy(newArray[c-found].addr, array[c].addr, INET_ADDRSTRLEN);
			if(array[c].fd > *fdMax)
				*fdMax = array[c].fd;
		}
		else
			found = 1;
	}

	free(array);
	(*size)--;
	return newArray;
}

void* handlerProc(void* arg){
	int fd = ((ClientInfo*)arg)->fd;
	char buffer[BUFFER_SIZE];

	// Read until request is processesed
	while(read(fd, buffer, BUFFER_SIZE) > 0){
		//if keep-alive is not set
		if(response((ClientInfo*)arg, &buffer[0]) == -1){
			shutdown(fd, 2);
			close(fd);
			free((ClientInfo*)arg);
			return (void*)0;
		}
		bzero(&buffer[0], BUFFER_SIZE);
	}

	shutdown(fd, 2);
	close(fd);
	free((ClientInfo*)arg);
	return (void*)0;
}

void threadLoop(int fd){
	pthread_t handler;
	while(1){
		struct sockaddr_in clientAddr;

		// Assigned memory for client info
		ClientInfo *clientInfo = (ClientInfo*)malloc(sizeof(ClientInfo));
		unsigned int addrLen = sizeof(clientAddr);

		// Accept client connection
		clientInfo->fd = accept(fd, (struct sockaddr*) &clientAddr, &addrLen);

		char str[INET_ADDRSTRLEN];
		struct in_addr ipAddr = clientAddr.sin_addr;
		inet_ntop(AF_INET, &ipAddr, str, INET_ADDRSTRLEN);
		strncpy(clientInfo->addr, str, INET_ADDRSTRLEN);

		if(!pthread_create(&handler, NULL, handlerProc, (void*)clientInfo)){
			// Thread is detached
			//pthread_detach(handler);
		}
		else{
			logError("Could not create thread...\n");
			return;
		}
	}
}

void forkLoop(int fd){
	signal(SIGCHLD, SIG_IGN);
	while(1){
		// Accept the connection
		struct sockaddr_in clientAddr;
		unsigned int addrLen = sizeof(clientAddr);
		int clientSock = accept(fd, (struct sockaddr*) &clientAddr, &addrLen);

		// Create child process
		int pid = fork();
		if(pid == 0){
			ClientInfo* clientInfo = (ClientInfo*)malloc(sizeof(ClientInfo));
			char str[INET_ADDRSTRLEN];
			struct in_addr ipAddr = clientAddr.sin_addr;
			inet_ntop(AF_INET, &ipAddr, str, INET_ADDRSTRLEN);
			strncpy(clientInfo->addr, str, INET_ADDRSTRLEN);
			clientInfo->fd = clientSock;
			handlerProc((void*)clientInfo);
			return;
		}
		else if(pid == -1){
			logError("Failed to create child process");
			return;
		}
		close(clientSock);
	}
}

void muxLoop(int fd){
	int size = 0;
	int fdMax = fd;
	int prevMax = 0;
	ClientInfo *fdArray;
	ClientInfo temp;
	temp.fd = fd;
	fdArray = fdPush(fdArray, &size, &temp, &fdMax);
	fd_set fdSet;
	while(1){
		struct timeval timeout;
		do{
			// Set file descriptor group
			FD_ZERO(&fdSet);
			for(int c = 0; c < size; c++)
				FD_SET(fdArray[c].fd, &fdSet);

			// Set select timeout
			timeout.tv_sec = 10;
			timeout.tv_usec = 0;

		}while(!select(fdMax+1, &fdSet, NULL, NULL, &timeout));

		// Test which fd is ready
		if(FD_ISSET(fd, &fdSet)){
			// A new connection is ready
			struct sockaddr_in clientAddr;
			unsigned int addrLen = sizeof(clientAddr);
			int clientSock = accept(fd, (struct sockaddr*) &clientAddr, &addrLen);

			// Set nonblocking in case there are nothing to recieve
			fcntl(clientSock, F_SETFL, fcntl(clientSock, F_GETFL, 0) | O_NONBLOCK);

			temp.fd = clientSock;
			char str[INET_ADDRSTRLEN];
			struct in_addr ipAddr = clientAddr.sin_addr;
			inet_ntop(AF_INET, &ipAddr, str, INET_ADDRSTRLEN);
			strncpy(temp.addr, str, INET_ADDRSTRLEN);
			fdArray = fdPush(fdArray, &size, &temp, &fdMax);
		}
		else{
			for(unsigned int c = 1; c < size; c++){
				if(FD_ISSET(fdArray[c].fd, &fdSet)){
					char buffer[BUFFER_SIZE];

					// Handle the request
					if(read(fdArray[c].fd, buffer, BUFFER_SIZE) > 0){
						if(response(&fdArray[c], &buffer[0]) == -1){
							shutdown(fdArray[c].fd, 2);
							close(fdArray[c].fd);
							fdArray = fdRemove(fdArray, &size, &fdArray[c], &fdMax);
						}
						bzero(&buffer[0], BUFFER_SIZE);
					}
					else{
						shutdown(fdArray[c].fd, 2);
						close(fdArray[c].fd);
						fdArray = fdRemove(fdArray, &size, &fdArray[c], &fdMax);
					}
				}
			}
		}
	}
}

void createRequestHandler(enum HandlingMethod handlingMethod, int fd){
	switch(handlingMethod){
		case FORK:
			forkLoop(fd);
		break;
		case THREAD:
			threadLoop(fd);
		break;
		case MUX:
			muxLoop(fd);
		break;
	}
}
