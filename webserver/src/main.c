#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <multipleRequests.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <request.h>
#include <configParser.h>
#include <logger.h>
#include <limits.h>

void printHelp(const char* message){
	if(message == NULL)
		printf("Usage:\n\t-h Print help text.\n\t-p port Listen to port number port.\n\t-d Run as a daemon instead of as a normal program.\n\t-l logfile Log to logfile. If this option is not specified, logging will be output to syslog, which is the default.\n\t-s [fork | thread | prefork | mux] Select request handling method.\n");
	else
		printf("ERROR: %s\n\nUsage:\n\t-h Print help text.\n\t-p port Listen to port number port.\n\t-d Run as a daemon instead of as a normal program.\n\t-l logfile Log to logfile. If this option is not specified, logging will be output to syslog, which is the default.\n\t-s [fork | thread | prefork | mux] Select request handling method.\n", message);
}

void daemonize(){
	if(fork() == 0){
		umask(0);
		if(setsid() < 0){
			logError("Unable to daemonize process...");
			exit(1);
		}
		printf("[%d] started\n", getpid());
		close(STDIN_FILENO);
		close(STDOUT_FILENO);
		close(STDERR_FILENO);
	}
	else
		// Close parent process
		exit(0);
}

char* catAlloc(const char* str1, const char* str2){
	int size = strlen(str1) + strlen(str2) + 1;
	char* res = malloc(size);
	strncpy(res, str1, size);
	strncat(res, str2, size);
	return res;
}

void jail(char *dir){
	gid_t gid;
	uid_t uid;

	const char *sudouid = getenv("SUDO_UID");
	if (sudouid == NULL) // Try to change effective uid
	{
		uid = getuid();
		seteuid(0);
	}
	else
		uid = (uid_t)strtoll(sudouid, NULL, 10);

	const char *sudogid = getenv("SUDO_GID");
	if (sudogid == NULL) // Try to change effective gid
	{
		gid = getgid();
		setegid(0);
	}
	else
		gid = (gid_t)strtoll(sudogid, NULL, 10);

	dir++;
	dir[strlen(dir) - 2] = 0;
	char *path = realpath(dir, NULL);

	chdir(path);
	if (chroot(path) != 0) {
		perror("chroot");
		exit(1);
	}
	
	setreuid(uid, uid);
	setregid(gid, gid);

	/* if (getuid() == 0)
	{
    if (setgid(groupid) != 0)
        fatal("setgid: Unable to drop group privileges: %s", strerror(errno));
    if (setuid(userid) != 0)
        fatal("setuid: Unable to drop user privileges: %S", strerror(errno));
	}

	if (setuid(0) != -1)
		fatal("ERROR: Managed to regain root privileges?");
 */

	//chdir("/tmp");
}

int main(int argc, const char* argv[]){
	logFile = NULL;
	logName = NULL;
	unsigned int port = 0;
	enum HandlingMethod handlingMethod = DEFAULT;
	char *end;

    // Handle the arguments
	for(unsigned int c = 1; c < argc; c++){
		if(strcmp(argv[c], "-q") == 0)
            return 0;
		else if(strcmp(argv[c], "-h") == 0){
			printHelp(NULL);
            return 0;
		}
		else if(strcmp(argv[c], "-p") == 0){
			c++;
			if(c >= argc){
				printHelp("Port is not specified...");
				return 0;
			}
			port = strtol(argv[c], &end, 10);
			if(port == 0 || port > 65535){
				;// Print help text
				return 0;
			}
		}
		else if(strcmp(argv[c], "-d") == 0)
            daemonize();
		else if(strcmp(argv[c], "-l") == 0){
			c++;
			if(c >= argc){
				printHelp("Logfile is not specified...");
				return 0;
			}
			char* filename = catAlloc(argv[c], ".log");
			logFile = fopen(filename, "a+");

			// Replace file extention
			memcpy(&filename[strlen(argv[c])], ".err", 4);
			logName = filename;
		}
		else if(strcmp(argv[c], "-s") == 0){
			c++;
			if(c >= argc){
				printHelp("Handling method is not specified...");
				return 0;
			}
			if(strcmp(argv[c], "fork") == 0)
				handlingMethod = FORK;
			else if(strcmp(argv[c], "thread") == 0)
				handlingMethod = THREAD;
			else if(strcmp(argv[c], "mux") == 0){
				handlingMethod = MUX;
			}
			else{
				printHelp("Unknown handling method...");
				return 0;
			}
		}
		else{
			printHelp("Unknown option...");
			return 3;
		}
	}
	// Read from config
	char serverRoot[MAX_STRLEN];
	parseConfig(".lab3-config", &port, &serverRoot[0], &handlingMethod);
	jail(serverRoot);

	// Create a listening socket
	int fd = socket(AF_INET, SOCK_STREAM, 0);
	struct sockaddr_in serverAddr;
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_addr.s_addr = INADDR_ANY;
	serverAddr.sin_port = htons(port);
	if(bind(fd, (struct sockaddr*) &serverAddr, sizeof(serverAddr)) == -1){
		logError("Unable to bind socket address...");
		close(fd);
			if(logFile != NULL)
			fclose(logFile);
		if(logName != NULL)
			free(logName);
	}
	listen(fd, 0);

	// Start the server loop
	createRequestHandler(handlingMethod, fd);

	// Close logfile if opened and free memory
	close(fd);
	if(logFile != NULL)
		fclose(logFile);
	if(logName != NULL)
		free(logName);
}
