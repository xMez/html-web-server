#include "request.h"

/* Send a HTTP Response and return the amount of data sent, -1 if keep-alive header is found */
ssize_t response(ClientInfo* clientInfo, char *buffer) // This will return the full-response ATM to be sent to the client
{
	// Error webpages
	char str400[] = "<html><head><link rel=\"icon\" href=\"data:;base64,iVBORw0KGgo=\">Bad Request</head><body><p>Error 400 - Bad Request</p></body></html>";
	char str403[] = "<html><head><link rel=\"icon\" href=\"data:;base64,iVBORw0KGgo=\">Forbidden</head><body><p>Error 403 - Forbidden</p></body></html>";
	char str404[] = "<html><head><link rel=\"icon\" href=\"data:;base64,iVBORw0KGgo=\">Not Found</head><body><p>Error 404 - Not Found</p></body></html>";
	char str500[] = "<html><head><link rel=\"icon\" href=\"data:;base64,iVBORw0KGgo=\">Internal Server Error</head><body><p>Error 500 - Internal Server Error</p></body></html>";
	char str501[] = "<html><head><link rel=\"icon\" href=\"data:;base64,iVBORw0KGgo=\">Not Implemented</head><body><p>Error 501 - Not Implemented</p></body></html>";

	char *fpath, *fbuf, *headers;
	type reqtype = {0, 0 ,0};
	logData data;

	int fd = clientInfo->fd;
	data.host = clientInfo->addr;
	data.ident = data.authUser = "-";

	if (strchr(buffer, 0) == NULL) // Ensure we have a null terminated string
		buffer[BUFFER_SIZE] = 0;

	char *req = strtok_r(buffer, "\r\n", &headers); // Split request from headers
	data.request = strdup(req);	
	data.status = parse_request(req, &fpath, &reqtype);
	reqtype.alive = parse_headers(headers);

	FILE *fp;
	if (fpath != NULL) // Try to open file, / opens index.html
	{
		if (strcmp(fpath, "/") == 0)
			fp = fopen("index.html", "rb");
		else
			fp = fopen(fpath, "rb");
		if (fp == NULL)	// File exists but cannot open
			data.status = 403;
	}

	int size;
	char moddate[DATE_SIZE] = {0};
	if (data.status == 200)
	{
		size = fsize(fp);
		struct stat s;
		fstat(fileno(fp), &s);
		time_t_gm_asc(&s.st_mtime, moddate);
	}
	else
		size = 150;

	char sizestr[F_SIZE];
	snprintf(sizestr, F_SIZE, "%d", size);

	char res[RES_SIZE] = {0};
	int ressize = 0;

	if (reqtype.full != 0 || reqtype.head == 1)
		header(data.status, sizestr, moddate, res, &ressize);

	if (reqtype.head != 1)
		body(data.status, size, fp, res, &ressize);

	data.bytes = send(fd, res, ressize, 0);

	if (fp != NULL)
		fclose(fp);

	logEvent(data);
	free(data.request);

	if (reqtype.alive == 0)
		return -1;

	return data.bytes;
}

/* Parse HTTP Request-Line */
int parse_request(char *req, char **fpath, type *reqtype)
{
	int ret = 200;
	char *saveptr;

	if (strchr(req, 0) == NULL) // Ensure we have a null terminated string
		req[BUFFER_SIZE] = 0;

	char *ptr = strtok_r(req, " ", &saveptr); // Get first word
	char *method = ptr;

	int pm;
	if(pm = parse_method(method, &reqtype->head) != 1)
		ret = pm; // 400 or 501


	ptr = strtok_r(NULL, " ", &saveptr); // Get next word
	char *URI = ptr;

	*fpath = parse_URI(URI);
	if(*fpath == NULL)
		ret = 404;

	ptr = strtok_r(NULL, " ", &saveptr);
	char *version = ptr;

	if (version != NULL) // If no version, simple request
	{
		reqtype->full = 1;
		if(!parse_version(version))
			ret = 400;
	}

	return ret;	
}

/* Parse HTTP Method */
int parse_method(char *method, int *head)
{
	int getcmp = strncmp(method, "GET", 3);
	int headcmp = strncmp(method, "HEAD", 4);
	int postcmp = strncmp(method, "POST", 4);
	
	if (headcmp == 0)
		*head = 1;

	if ((getcmp == 0) ^ (headcmp == 0))
		return 1;
	else if (postcmp == 0)
		return 501;
	else
		return 400;
}

/* Check if URI is a real path */
char *parse_URI(char *path)
{
	char *res = realpath(path, NULL);
	return res;
}

/* Parse HTTP version, supports 1.0 and 1.1 */
int parse_version(char *version)
{
	int http10 = strncmp(version, "HTTP/1.0", 10);
	int http11 = strncmp(version, "HTTP/1.1", 10);
	
	if ((http10 == 0) ^ (http11 == 0))
		return 1;
	else
		return 0;
}

/* Parse HTTP headers, checks if keep-alive exists */
int parse_headers(char *headers)
{
	if (strstr(headers, "Connection: keep-alive\r\n"))
		return 1;
	else
		return 0;
}

/* Convert a time_t variable to GMT and outputs to a buffer *buf */
void *time_t_gm_asc(time_t *atime, char *buf)
{
	struct tm gtime;
	// Thread safe
	gmtime_r(atime, &gtime);
	asctime_r(&gtime, &buf[0]);
}

/* Get size of FILE */
int fsize(FILE *fp)
{
	fseek(fp, 0L, SEEK_END); // move to EOF
	int size = ftell(fp); // get size
	rewind(fp);
	return size;
}

/* Create HTTP headers for a given HTTP status and output it to a buffer res */
void *header(int req, char *size, char *moddate, char *res, int *ressize)
{
	memcpy(&res[*ressize], "HTTP/1.0 ", 9);
	*ressize += 9;

	switch (req)
	{
	case 200:
		memcpy(&res[*ressize], "200 OK\r\n", 8);
		*ressize += 8;
		break;
	case 400:
		memcpy(&res[*ressize], "400 Bad Request\r\n", 17);
		*ressize += 17;
		break;
	case 403:
		memcpy(&res[*ressize], "403 Forbidden\r\n", 15);
		*ressize += 15;
		break;
	case 404:
		memcpy(&res[*ressize], "404 Not Found\r\n", 15);
		*ressize += 15;
		break;
	case 500:
		memcpy(&res[*ressize], "500 Internal Server Error\r\n", 27);
		*ressize += 27;
		break;
	case 501:
		memcpy(&res[*ressize], "501 Not Implemented\r\n", 21);
		*ressize += 21;
		break;
	
	default:
		break;
	}

	memcpy(&res[*ressize], "Date: ", 6);
	*ressize += 6;

	time_t now;
	now = time(NULL);
	char date[DATE_SIZE] = {0}; 
	time_t_gm_asc(&now, date);
	memcpy(&res[*ressize], date, strlen(date));
	*ressize += strlen(date);

	memcpy(&res[*ressize - 1], " GMT\r\n", 6);
	*ressize += 5;

	memcpy(&res[*ressize], "Content-Type: text/html\r\nContent-Length: ", 41);
	*ressize += 41;

	memcpy(&res[*ressize], size, strlen(size));
	*ressize += strlen(size);

	memcpy(&res[*ressize], "\r\nLast-Modified: ", 17);
	*ressize += 17;

	if (req == 200)
	{
		memcpy(&res[*ressize], moddate, strlen(moddate));
		*ressize += strlen(moddate);
	}
	else
	{
		memcpy(&res[*ressize], date, strlen(date));
		*ressize += strlen(date);
	}

	memcpy(&res[*ressize - 1], " GMT\r\n", 6);
	*ressize += 5;

	memcpy(&res[*ressize], "Server: cserver\r\n\r\n", 19);
	*ressize += 19;
}

/* Create HTTP Entity-Body and output to a buffer res */
void *body(int req, int size, FILE *fp, char *res, int *ressize)
{
	char *bod;
	switch (req)
	{
	case 200:
		bod = malloc(size);
		if (bod)
			fread(bod, 1, size, fp);
		break;
	case 400:
		bod = "<html><head><link rel=\"icon\" href=\"data:;base64,iVBORw0KGgo=\">Bad Request</head><body><p>Error 400 - Bad Request                    </p></body></html>";
		break;
	case 403:
		bod = "<html><head><link rel=\"icon\" href=\"data:;base64,iVBORw0KGgo=\">Forbidden</head><body><p>Error 403 - Forbidden                        </p></body></html>";
		break;
	case 404:
		bod = "<html><head><link rel=\"icon\" href=\"data:;base64,iVBORw0KGgo=\">Not Found</head><body><p>Error 404 - Not Found                        </p></body></html>";
		break;
	case 500:
		bod = "<html><head><link rel=\"icon\" href=\"data:;base64,iVBORw0KGgo=\">Internal Server Error</head><body><p>Error 500 - Internal Server Error</p></body></html>";
		break;
	case 501:
		bod = "<html><head><link rel=\"icon\" href=\"data:;base64,iVBORw0KGgo=\">Not Implemented</head><body><p>Error 501 - Not Implemented            </p></body></html>";
		break;
	
	default:
		break;
	}
	memcpy(&res[*ressize], bod, size);
	*ressize += size;
	if (req == 200)
		free(bod);
}