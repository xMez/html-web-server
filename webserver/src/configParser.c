#include <configParser.h>

int parseConfig(const char* filename, int *port, char *serverRoot, enum HandlingMethod *handlingMethod){
	FILE *configFile = fopen(filename, "r");
	char attribute[MAX_STRLEN], value[MAX_STRLEN];

	// Loop through lines
	while(fscanf(configFile, " %s ", &attribute[0]) >= 0){

		// Check if line is a comment
		if(strcmp(attribute, "#") == 0)
			fgets(attribute, MAX_STRLEN, configFile);
		else{
			char *end;
			fgets(value, MAX_STRLEN, configFile);
			if(strcmp(attribute, "port") == 0 && *port == 0)
				*port = strtol(value, &end, 10);
			else if(strcmp(attribute, "server-root") == 0)
				strcpy(serverRoot, value);
			else if(strcmp(attribute, "handling-method") == 0 && *handlingMethod == DEFAULT){
				if(strcmp(value, "thread\n") == 0)
					*handlingMethod = THREAD;
				else if(strcmp(value, "fork\n") == 0)
					*handlingMethod = FORK;
				else if(strcmp(value, "mux\n") == 0)
					*handlingMethod = MUX;
				else{
					// Log error in config file
					logError("Broken config file...");
				}
			}
		}
	}
	fclose(configFile);
}