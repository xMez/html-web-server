#include <logger.h>

void logEvent(logData data){
	// Get current time
	time_t timeData;
	time(&timeData);
	struct tm *timeInfo = localtime(&timeData);
	char buffer[TIME_SIZE];
	strftime(buffer, TIME_SIZE, "%d/%b/%Y:%H:%M:%S %z", timeInfo);

	// Check if log message is too long
	if(strlen(data.host) + strlen(data.ident) + strlen(data.authUser) + strlen(data.request) + TIME_SIZE > MAX_LOG_MESSAGE){
		logError("Log message is too long...");
		return;
	}
	
	// If syslog is to be used
	if(logFile == NULL){
		// Write to syslog
		openlog(NULL, 0, LOG_USER);
		syslog(LOG_INFO | LOG_USER, "%s %s %s [%s] \"%s\" %d %ld\n", data.host, data.ident, data.authUser, buffer, data.request, data.status, data.bytes);
		closelog();
		return;
	}

	// Write fields to log file
	fprintf(logFile, "%s %s %s [%s] \"%s\" %d %ld\n", data.host, data.ident, data.authUser, buffer, data.request, data.status, data.bytes);
	fflush(NULL);
}

void logError(const char* errorMessage){
	// Get current time
	time_t timeData;
	time(&timeData);
	struct tm *timeInfo = localtime(&timeData);
	char buffer[TIME_SIZE];
	strftime(buffer, TIME_SIZE, "%d/%b/%Y:%H:%M:%S %z", timeInfo);

	// Check if message is too long
	if(TIME_SIZE + strlen(errorMessage) > MAX_LOG_MESSAGE){
		logError("Log message is too long...");
		return;
	}

	// If syslog is to be used
	if(logName == NULL){
		// Write to syslog
		openlog(NULL, 0, LOG_USER);
		syslog(LOG_ERR | LOG_USER, "[%s] %s\n", buffer, errorMessage);
		closelog();
		return;
	}

	// Log to file
	FILE *file = fopen(logName, "a+");
	if(file == NULL)
		return;
	fprintf(file, "[%s] %s\n", buffer, errorMessage);
	fclose(file);
}